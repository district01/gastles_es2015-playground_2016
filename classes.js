
import User from "./modules/User.js";

const JOBS = {
  CTO: 'Chief Technical Officer',
  CEO: 'Chief Executive Officer',
  DEV: 'Developer'
};



class Employee extends User {

  constructor(name, age, job) {
    super(name, age);

    this._job = job;
  }

  get job() {
    return this._job;
  }

  set job(job) {
    this._job = job;
  }

  toString() {
    return `${this.name} is ${this.age} and works as ${this.job}`;
  }
}

var user = new User('Ash', 25);
var employee = new Employee('Brock', 23, JOBS.DEV);

console.log(user.name);
user.name = 'Misty';
console.log(user.toString())
console.log('\n');

console.log(employee.name);
employee.job = JOBS.CTO;
console.log(employee.toString())

console.log('\n');
