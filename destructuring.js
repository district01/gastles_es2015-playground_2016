import { green, bold } from "chalk";

// the gist of it
var someArray = ["one", "two", "three"];

// vroeger
var first = someArray[0];
var second = someArray[1];
var third = someArray[2];

console.log(green`vroeger`);
console.log(`
    first: ${first}
    second: ${second}
    third: ${third}
`);

// nu
var [first2, second2, third2] = someArray;

console.log(green`nu`);
console.log(`
    first2: ${first2}
    second2: ${second2}
    third2: ${third2}
`);

// or for objects
var someObject = {
  name: "New Media"
}

var {name} = someObject;

console.log(green`With objects`);
console.log(`
    name: ${name}
`);
