import { green, bold } from "chalk";
import gameEvents from "./modules/gameEvents.js";
const log = console.log;

const message = `Let's play some footsketball
The game event log is in, try to show us the score board for team 1 and 2
`;

const teams = [1, 2];
const setEvents = new Set(gameEvents);

log(green.bold(message));

function calculateScore(team) {
    return setEvents.toArray()
        .filter(event => event.type === 'goal')
        .filter(event => event.team === team)
        .map(event => event.points)
        .reduce((previous = 0, value = 0) => previous + value);
}

teams.forEach((team) => log(`Team ${team} scored ${calculateScore(team)} points`));