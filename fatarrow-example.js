import gameEvents from "./modules/gameEvents";
import {log} from "./modules/logger";

const teams = [1, 2];

const getNumberOfGoalsPerTeam = function getNumberOfGoalsPerTeam(team) {
    return gameEvents
        .filter(event => event.type === 'goal')
        .filter(event => event.team === team)
        .map(event => event.points)
        .reduce((previous, value) => (previous || 0) + value);
}

teams.forEach(team => log(`Team: ${team} scored ${getNumberOfGoalsPerTeam(team)} times`));

