import { green, bold } from "chalk";
import _ from "lodash";

function add10(val) {
  console.log(`\tAdding 10 to ${val} gives us ${val+10}`);
}

function log(...values) {
  console.log('\t', ...values);
}

console.log(green('No argument'));

var arr1 = [1,2,3,4,5];
_.forEach(arr1, () => log("Hello New Media"));


console.log(green('\nSingle argument'));
var arr = [1,2,3,4,5,6,7,8,9,10];
_.forEach(arr, val => add10(val));


console.log(green('\nMultiple arguments'));
var complex = [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]];
_.forEach(complex, (value1, value2) => log(value1, value2));


console.log(green('\nMultiple arguments and more than 1 statement'));
var complex2 = [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]];
_.forEach(complex2, (args) => {
  log(`Hello New Media...`);
  log(...args);
});


console.log(green('\nThe context of This'));
function User() {
 this.name = 'Sander';
 setTimeout(() => {
  this.name = 'Hello World!'; // properly refers to the User
 }, 1000);

 this.print = function () {
   console.log('\t' + bold(`${this.name}`));
 }
}

var u = new User();
u.print();

setTimeout(() => {
  u.print();
 }, 2000);
