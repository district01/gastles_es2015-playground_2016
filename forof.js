import { green, bold } from 'chalk';

var source = "new media".split('');
Array.prototype.newMethod = function(args) {
  // do shizzle
};
source.myCustomProperty = 'Whiiii';

// Example for in like we had in ES5
console.log(green.bold`\nIn ES5, the for in construct was used to loop over an array\'s items\n`);
for (let s in source) {
  let value = source[s];
  console.log(`  Yay ${bold(s)}: ${green(value)}`);
}

// Example for in like we had in ES6
console.log(green.bold`\nIn ES6, the for in construct was used to loop over an array\'s items\n`);
for (let s of source) {
  console.log(`  Yay: ${green(s)}`);
}
