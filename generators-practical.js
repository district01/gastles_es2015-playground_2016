import { box, green, bold, greenbox, boldgreenbox } from './modules/helper';
import fetch from 'node-fetch';
import co from 'co';

console.log('\n' + boldgreenbox('  Some practical use...                   ') + '\n');

const url = 'http://jsonplaceholder.typicode.com/photos/1';

console.log(`fetching ${url}\n`);

fetch(url)
  .then(response => response.json())
  .then(photo => photo.title)
  .then(x => console.log(green('Title via promises:\n'), x));

co(function *() {
    const response = yield fetch(url);
    const photo = yield response.json();
    const title = photo.title;
    console.log(green('Title via generators:\n'), title);
});
