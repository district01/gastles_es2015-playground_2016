import { green, bold } from "chalk";
import box from "boxen";

console.log('\n' + box('  Bas  ic generator                       '));

function *counter() {
    yield 1;
    yield 2;
    yield 3;
    yield 4;
    yield 5;
    return 'final result';
}

const iterator = counter();
console.log('iterator: ', iterator);

console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());

// ---------------------------------------

console.log('\n' + box('  Looping over the results                '));

function *counter2() {
    yield 1;
    yield 2;
    yield 3;
    yield 4;
    yield 5;
    return 'All done!';
}

const iterator2 = counter2();
console.log('iterator2: ', iterator2);
let result2 = iterator2.next();
console.log(result2);
while(result2.done === false) {
  console.log('temporary result: ', result2.value);
  result2 = iterator2.next();
}
console.log('final result:', result2.value);

// ---------------------------------------
