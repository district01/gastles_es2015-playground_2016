import { green, bold } from 'chalk';

console.log(green('Creating maps & Setting values'));
let map = new Map();

let value2 = 'value2';
let someValue = {
  key: 'value'
};
const myKey = {};

map.set(0, 'value1');
map.set('1', value2);
map.set({ key: 2 }, someValue);
map.set(myKey, 'complex keys');

console.log('map:', map);

let map2 = new Map([
  [0, 'value1'],
  ['1', value2],
  [{ key: 2 }, someValue],
  [myKey, 'complex keys']
]);

console.log('map2:', map2);

console.log(green('\nGetting values'));

console.log(`Key: '1' has value: ${map2.get('1')}`);
console.log(`Key: myKey has value: ${map2.get(myKey)}`);

console.log(green('\nLooping over values: ' + bold('forEach')));

map.forEach(function (value, key) {
  console.log(`Key: ${key} has value: ${value}`);
});

console.log(green('\nLooping over values: ' + bold('for..of')));
for (let entry of map) {
  console.log(`Key: ${entry[0]} has value: ${entry[1]}`);
};

console.log(green('\nMap properties: ' + bold('map.size')));
console.log(`map.size: ${map.size}`);

console.log(green('\nMap methods: ' + bold('map.has(key)')));
console.log(`Does map have mykey? ${map.has(myKey)}`);

console.log(green('\nMap methods: ' + bold('map.entries()')));
console.log(map.entries());

console.log(green('\nMap methods: ' + bold('map.keys()')));
console.log(map.keys());

console.log(green('\nMap methods: ' + bold('map.values()')));
console.log(map.values());

console.log(green('\nMap methods: ' + bold('map.delete()')));
map.delete('1');
console.log(`map after we deleted '1'`, map);
console.log(`map size after deleting a key ${map.size}`)

console.log(green('\nMap methods: ' + bold('map.clear()')));
map.clear();
console.log(`did we clear map?`, map);


