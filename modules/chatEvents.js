const chatEvents = [
    {
        channel: "newmedia",
        time: "May 13th at 09:08:24",
        user: "nicola",
        type: "file",
        title: "Enkele sfeerbeelden ter vervanging van de missing snapchat story",
        url: "https://files.slack.com/files-pri/T025KRL03-F18JZJAFN/download/yesterday.mp4"
    },
    {
        channel: "newmedia",
        time: "May 13th at 10:18:55",
        user: "laurens",
        type: "message",
        message: "die conferentie is in een cafe ofzo?"
    },
    {
        channel: "newmedia",
        time: "May 13th at 10:49:47",
        user: "fabian",
        type: "message",
        message: "D01 heeft basically Thomas, Nico en Kevin hunne Pub Crawl gesponsord :hatersgonnahate:",
        reactions: [{
            amount: 2,
            emoticon: ":hatersgonnahate:"
        }, {
            amount: 3,
            emoticon: ":umadbrah:"
        }, {
            amount: 2,
            emoticon: ":monkey-dance:"
        }]
    },
    {
        channel: "newmedia",
        time: "May 13th at 10:50:57",
        user: "thomas",
        type: "message",
        message: "Conf is in een dancing :stuck_out_tongue: #nojoke",
        reactions: [{
            amount: 2,
            emoticon: ":beers:"
        }, {
            amount: 1,
            emoticon: ":dancer:"
        }, {
            amount: 2,
            emoticon: ":dancers:"
        }, {
            amount: 2,
            emoticon: ":monkey-dance:"
        }]
    },
    {
        channel: "newmedia",
        time: "May 13th at 10:52:37",
        user: "fabian",
        type: "message",
        message: "Het enige da Thomas geleerd gaat hebben zijn massive playa skills",
        reactions: [{
            amount: 2,
            emoticon: ":unamused:"
        }, {
            amount: 1,
            emoticon: ":dark_sunglasses:"
        }, {
            amount: 2,
            emoticon: ":monkey-dance:"
        }]
    },
    {
        channel: "newmedia",
        time: "May 13th at 10:52:55",
        user: "fabian",
        type: "message",
        message: "én CSS selectors :point_up:",
        reactions: [{
            amount: 5,
            emoticon: ":+1:"
        }, {
            amount: 3,
            emoticon: ":css:"
        }, {
            amount: 2,
            emoticon: ":monkey-dance:"
        }]
    },
    {
        channel: "newmedia",
        time: "May 13th at 10:53:11",
        user: "fabian",
        type: "message",
        message: "Hebde uwen talk over Cordova al gegeven? :hatersgonnahate:",
        reactions: [{
            amount: 1,
            emoticon: ":-1:"
        }, {
            amount: 1,
            emoticon: ":middle_finger:"
        }, {
            amount: 2,
            emoticon: ":cordova:"
        }, {
            amount: 1,
            emoticon: ":loudspeaker:"
        }, {
            amount: 2,
            emoticon: ":monkey-dance:"
        }]
    },
    {
        channel: "newmedia",
        time: "May 13th at 11:18:10",
        user: "sander",
        type: "message",
        message: "dees channel heeft meer kleurprentjes dan mijn editie van 'Tiny gaat kamperen'",
        reactions: [{
            amount: 2,
            emoticon: ":joy:"
        }, {
            amount: 1,
            emoticon: ":camping:"
        }, {
            amount: 1,
            emoticon: ":tent:"
        }, {
            amount: 1,
            emoticon: ":hatersgonnahate:"
        }]
    },
    {
        channel: "newmedia",
        time: "May 13th at 11:19:38",
        user: "fabian",
        type: "message",
        message: "Gij hebt een editie van 'Tiny gaat kamperen'?"
    },
    {
        channel: "newmedia",
        time: "May 13th at 11:19:48",
        user: "koen.everaert",
        type: "message",
        message: "De blinkende zelfs :wink:"
    },
    {
        channel: "newmedia",
        time: "May 13th at 11:20:36",
        user: "sander",
        type: "message",
        message: "of course, ooit had ik ze allemaal"
    },
    {
        channel: "newmedia",
        time: "May 13th at 11:52:59",
        user: "vodde",
        type: "message",
        message: "ik ook :s"
    },
    {
        channel: "newmedia",
        time: "May 13th at 14:49:50",
        user: "gaitano",
        type: "message",
        message: "Wallmateriaal"
    }
];

export default chatEvents;