const gameEvents = [
    {
        team: 1,
        points: 1,
        time: "May 13th at 21:08:24",
        user: "marc",
        type: "goal"
    },
    {
        team: 2,
        time: "May 13th at 21:14:55",
        user: "sander.geenen",
        type: "card",
        color: "yellow"
    },
    {
        team: 2,
        points: 3,
        time: "May 13th at 21:19:47",
        user: "fabian",
        type: "goal"
    },
    {
        team: 1,
        points: 1,
        time: "May 13th at 21:21:57",
        user: "koen.everaert",
        type: "goal"
    },
    {
        team: 2,
        points: 1,
        time: "May 13th at 21:23:15",
        user: "sander.geenen",
        type: "goal"
    },
    {
        team: 1,
        points: 1,
        time: "May 13th at 21:28:55",
        user: "laurens",
        type: "goal"
    },
    {
        team: 2,
        points: 1,
        time: "May 13th at 21:31:11",
        user: "hans",
        type: "goal"
    },
    {
        team: 1,
        points: 1,
        time: "May 13th at 21:37:10",
        user: "mike",
        type: "goal"
    },
    {
        team: 2,
        time: "May 13th at 21:40:37",
        user: "sander.geenen",
        type: "card",
        message: "yellow"
    },
    {
        team: 2,
        time: "May 13th at 21:40:37",
        user: "sander.geenen",
        type: "card",
        message: "red",
        remark: "bots!"
    },
    {
        team: 1,
        points: 3,
        time: "May 13th at 21:45:38",
        user: "joris",
        type: "goal"
    },
    {
        team: 2,
        points: 1,
        time: "May 13th at 21:49:48",
        user: "willem",
        type: "goal"
    },
    {
        team: 2,
        points: 3,
        time: "May 13th at 21:52:36",
        user: "fabian",
        type: "goal"
    },
    {
        team: 1,
        points: 1,
        time: "May 13th at 21:57:59",
        user: "marc",
        type: "goal"
    }
];

export default gameEvents;