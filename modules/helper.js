import box from 'boxen';
import {green, bold} from 'chalk';

export const greenbox = (x) => green(box(x));
export const boldgreenbox = (x) => bold(greenbox(x));
export {green, bold, box};
