/* log method */
export function log(value, ...values) {
    console.log('\t', value);

    if (values.length > 0) {
        log(...values);
    }
};

