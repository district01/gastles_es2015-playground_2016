import { box, green, bold, greenbox, boldgreenbox } from './modules/helper';
import fetch from 'node-fetch';

console.log('\n' + boldgreenbox('  Promise all example...                   ') + '\n');

const url = 'http://jsonplaceholder.typicode.com/photos/1';
const url2 = 'http://jsonplaceholder.typicode.com/posts/3';

console.log(`fetching ${url}\n`);

const p1 = Promise.resolve(3); // immediate resolved
const p2 = 1337; // promise resolves this also immediately
const p3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, "foo"); // will be resolved after 1000 ms
});
const p4 = fetch(url); // will resolve after fetch
const p5 = fetch(url2); // will resolve after fetch

Promise.all([p1, p2, p3, p4, p5]).then(values => {
  const [v1,v2,v3,v4,v5] = values;  // destructuring values in separate vars
    console.log(v1); // result
    console.log(v2); // result
    console.log(v3); // result

    // v4 and v5 are values from fetch (we want the json result of it, but that is a promise in and of itself)
    Promise.all([v4.json(), v5.json()]).then(values => {
      const [json4, json5] = values;
      console.log(json4.title);
      console.log(json5.title);

    });

  // values.map(v => console.log(v));
});
