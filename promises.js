import { box, green, bold, greenbox, boldgreenbox } from './modules/helper';
import fetch from 'node-fetch';

console.log('\n' + boldgreenbox('  Some promise example...                   ') + '\n');

const url = 'http://jsonplaceholder.typicode.com/photos/1';

console.log(`fetching ${url}\n`);

fetch(url)
  .then(response => response.json())
  .then(photo => photo.title)
  .then(x => console.log(green(`Title via promises:\n${bold(x)}`)));
