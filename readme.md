# esbox demo app

## dependencies

```
(sudo) npm install -g esbox
```

## usage

open a terminal, and navigate to the directory you are working in. then start esbox on 1 of your files you are playing in by issuing the following command: `esbox myfile.js`
