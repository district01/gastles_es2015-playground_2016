import { green, bold } from 'chalk';

// fetching rest params

function containsAll(haystack, ...needles) {
  for (var needle of needles) {
    if (haystack.indexOf(needle) === -1) {
      return false;
    }
  }
  return true;
}

var source = green('new media');

console.log(`does '${source}' contain 'n' && 'dia'?\n`,
  containsAll(source, 'n', 'dia'));

console.log(`does '${source}' contain 'n' && 'dia' && 'new'?\n`,
  containsAll(source, 'n', 'dia', 'new'));
