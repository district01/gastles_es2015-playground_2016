import { green, bold } from 'chalk';

console.log(green('Creating sets & Setting values'));
let set = new Set();

let value2 = 'value2';
let someValue = {
  key: 'value'
};
const myKey = {};

set.add(1);
set.add('1');
set.add(value2);
set.add(someValue);

console.log('set:', set);

let set2 = new Set([
  1,
  '1',
  value2,
  someValue,
]);

console.log('set2:', set2);

console.log(green('\nLooping over values: ' + bold('forEach')));

set.forEach(function (value) {
  console.log(`Value: ${value}`);
});

console.log(green('\nLooping over values: ' + bold('for..of')));
for (let entry of set) {
  console.log(`Value: ${entry}`);
};

console.log(green('\nset properties: ' + bold('set.size')));
console.log(`set.size: ${set.size}`);

console.log(green('\nset does not allow duplicates'));
set.add(1);
set.add(1);
set.add(1);
set.add(1);
console.log(`We tried to add 1 a few more times... set.size: ${set.size}`);

console.log(green('\nset methods: ' + bold('set.has(value)')));
console.log(`Does set have '1'? ${set.has('1')}`);

console.log(green('\nset methods: ' + bold('set.delete()')));
set.delete('1');
console.log(`set after we deleted '1'`, set);
console.log(`set size after deleting a value ${set.size}`)

console.log(green('\nset methods: ' + bold('set.clear()')));
set.clear();
console.log(`did we clear set?`, set);


